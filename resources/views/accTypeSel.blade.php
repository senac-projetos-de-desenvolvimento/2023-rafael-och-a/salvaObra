<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Rafael Ochôa Mello">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

    <style>
        .carousel-control-next,
        .carousel-control-prev {
            filter: invert(100%);
        }
    </style>

    <title>Salva Obra</title>
</head>

<body id="userType-selection">
    <div id="userType-title-area">
        <div id="userType-title-container">
            <p>Tipo de usuário</p>
        </div>
    </div>

    <!-- Carousel -->
    <div id="carouselExample" class="carousel slide">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div id="userType-center-area">
                    <div
                        style="
                    display:flex;
                    flex-flow: column nowrap;
                    align-items: center;
                    ">
                        <img id="userType-image" src="{{ url('images/client-type-selection.png') }}"
                            alt="Usuario-Tipo-Cliente" srcset="" style="width: 230px; height: auto;">
                        <div id="userType-footer-container">
                            <a href="{{ route('register') }}">
                                <button id="userType-select-button" type="button">Cliente</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div id="userType-center-area">
                    <div
                        style="
                display:flex;
                flex-flow: column nowrap;
                align-items: center;
                ">
                        <img id="userType-image"src="{{ url('images/worker-type-selection.png') }}"
                            alt="Usuario-Tipo-Prestador" srcset="" style="width: 400px; height: auto;">
                        <div id="userType-footer-container">
                            <a href="{{ route('worker.create') }}">
                                <button id="userType-select-button" type="button">Prestador</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <!-- Scripts :: Bootstrap -->
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
</body>

</html>
