<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Rafael Ochôa Mello">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <title>Salva Obra</title>
</head>

<body id="loginPage">
    <div id="login-container">
        <img src="{{ url('images/Title.png') }}" alt="SalvaObra" srcset="" id="login-image-title">

        <form method="POST" action="{{ route('login') }}">
            @csrf

            {{-- E-mail --}}
            <div class="input-login-container">
                <div class="input-login-helper">
                    <label for="email" class="input-login-label col-form-label"
                        id="input-login-label-email">{{ _('E-mail') }}</label>

                    <input type="email" name="email" id="email"
                        class="login-input login-email form-control @error('email') is-invalid @enderror"
                        placeholder="seu@email.com.br" value="{{ old('email') }}" required autocomplete="email"
                        autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            {{-- Password --}}
            <div class="input-login-container">
                <div class="input-login-helper">

                    <label for="password" class="input-login-label"
                        id="input-login-label-password">{{ _('Senha') }}</label>

                    <input type="password" name="password" id="password"
                        class="login-password login-input form-control @error('password') is-invalid @enderror"
                        placeholder="•••••••••••••••" required autocomplete="current-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    {{-- Recuperar senha --}}
                    @if (Route::has('password.request'))
                        <div style="display: flex; flex-flow: row-reverse nowrap;">
                            <a href="{{ route('password.request') }}"
                                id="login-recover-password">{{ __('Recuperar senha') }}</a>
                        </div>
                    @endif

                </div>
            </div>

            {{-- Buttons --}}
            <div class="input-login-container">
                <div class="input-login-helper">
                    <button id="login-button" type="submit">{{ __('Entrar') }}</button>
                    <a href="{{ route('accTypeSel') }}">
                        {{-- <a href="{{ route('register') }}"> --}}
                        <button id="new-account-button" type="button">Nova Conta</button>
                    </a>
                </div>
            </div>
        </form>
    </div>
</body>

</html>

{{-- Original Implementation --}}

{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
