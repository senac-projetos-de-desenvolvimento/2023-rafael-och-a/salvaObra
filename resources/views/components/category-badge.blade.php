@isset($category)
    @if ($category !== 'invalid')
        <div class="catBadge-container">
            <div class="catBadge {{ $profileText == true ? 'catBadge-shadow' : ' ' }}">
                <img src="{{ url("images/icons/$category.svg") }}" alt="Icone de {{ $category }}"
                    style="height: {{ $iconH }}; width: {{ $iconW }};" />
            </div>
            @if ($hasName)
                <p class="{{ $profileText == true ? 'cat-badge-text-p' : 'cat-badge-text-l' }}">{{ $category }}</p>
            @endif
        </div>
    @endif
@endisset
