{{-- @if (count($categories) > 0) --}}
<div class="workerCard">
    <div class="workerCard-img-area">
        {{-- Temporario, utilizado para que não fique vazio com as seeds --}}
        <img src="https://picsum.photos/seed/{{$randomImgSeed}}/250/200" style="width: 100%" alt="">
    </div>
    <div class="workerCard-content-area">
        <section class="workerCard-content-data">
            <p class="workerCard-title">{{ $title }}</p>
            <p class="workerCard-adress">{{ $adress }}</p>
            <p class="workerCard-phone">{{ $phone }}</p>
        </section>
        <footer class="workerCard-footer">
            <div style="display: flex; flex-flow: row nowrap; margin-bottom: 0px; gap:4px">
                @isset($categories)
                    @if ($badgeLimit)
                        @for ($i = 0; $i < 2; $i++)
                            <x-category-badge :category="$categories[$i]" :hasName=true :profileText=false />
                        @endfor
                        <x-category-badge :category="'outros'" :hasName=true :profileText=false />
                    @else
                        @foreach ($categories as $cat)
                            <x-category-badge :category="$cat" :hasName=true :profileText=false />
                        @endforeach
                    @endif
                @endisset
            </div>
            <section class="workerCard-footer-score">
                <p class="workerCard-score-value">{{ $score }}</p>
                <p class="workerCard-score-nota">Nota</p>
            </section>
        </footer>
    </div>
</div>
{{-- @endif --}}
