@extends('templates/generalPagesTemplate')

@section('worker-searchBar')
    <div style="margin-top: 15px;">
        <form action="" method="get">
            <input type="text" name="search" id="search" value="{{ $search !== null ? $search : '' }}"
                class="worker-search-bar" placeholder="pesquisar..">
            <button type="submit" class="btn btn-outline-light worker-search-button">Pesquisar</button>
        </form>
    </div>
@endsection

@section('l-head-title')
    <p>Ordenar</p>
@endsection

@section('GPT-container-l')
    <div class="GPT-container-l-a-container">
        <a href="{{ route('worker.az') }}" class="GPT-container-l-a">A-Z</a>
        <a href="{{ route('worker.za') }}" class="GPT-container-l-a">Z-A</a>
        <a href="{{ route('worker.index') }}" class="GPT-container-l-a">Nota</a>
        <a href="#" class="GPT-container-l-a">No Bairro</a>
        <a href="#" class="GPT-container-l-a">Favoritos</a>
    </div>
    <header class="GPT-container-l-head" style="border-radius: 0px !important; margin-top: 50px;">
        <p>Filtrar</p>
    </header>
    <div class="GPT-container-l-a-container">
        <button type="button" data-bs-toggle="modal" data-bs-target="#categoryModal"
            style="border: none; margin-top: 20px; background-color: transparent;">
            <a href="#" class="GPT-container-l-a">Categorias</a>
        </button>
        <a href="#" class="GPT-container-l-a">Bairro</a>
    </div>
@endsection

@section('GPT-container-r')
    <div
        style="display: flex; flex-flow: row wrap; width: 100%; height: 100%; justify-content: space-evenly; overflow: auto">
        @if (count($workers) <= 0)
            <p>Nenhum encontrado!</p>
        @endif
        @foreach ($workers as $worker)
            <a href="{{ route('worker.show', $worker->id) }}" style="text-decoration: none;">
                <x-worker-card :image="$worker->image?->src" :neighbor=false :title="$title = $worker->user->name" :adress="$worker->user->address()?->street" :phone="$worker->work_phone"
                    :categories="$worker->categories" :score="$worker->rate" :fromJsonArray="true" />
            </a>
        @endforeach
    </div>
    {{-- <p>{{count($workers)}}</p> --}}
    {{ $workers->links() }}
@endsection

<!-- Modal -->
<div class="modal fade" id="categoryModal" tabindex="-1" aria-labelledby="categoryModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-4" style="border:none !important; box-shadow: rgba(0, 0, 0, 0.5) 0px 3px 8px;">
            <div class="modal-header border-0">
                <h1 class="modal-title fs-3" id="categoryModalLabel" style="color:#4b4b4b;">Escolha o serviço</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body center">
                <form action="" method="get">
                    <div
                        style="display:flex; flex-flow:row wrap; justify-content: center; row-gap: 50px; column-gap: 20px; margin-bottom: 50px; margin-top: 20px;">

                        {{-- Col-esq --}}
                        <div style="display: flex; flex-flow:column wrap; gap:20px;">

                            {{-- Servente --}}
                            <input type="radio" class="btn-check" name="catOpt" id="servente" autocomplete="off" checked
                                value="11">
                            <label class="btn btn-outline-dark worker-modal-btn" for="servente">Servente</label>

                            {{-- Arquitetura --}}
                            <input type="radio" class="btn-check" name="catOpt" id="arquitetura" autocomplete="off"
                                value="1">
                            <label class="btn btn-outline-dark worker-modal-btn" for="arquitetura">Arquitetura</label>

                            {{-- Encanamento --}}
                            <input type="radio" class="btn-check" name="catOpt" id="encanamento" autocomplete="off"
                                value="5">
                            <label class="btn btn-outline-dark worker-modal-btn" for="encanamento">Encanamento</label>

                            {{-- Marcenaria --}}
                            <input type="radio" class="btn-check" name="catOpt" id="marcenaria" autocomplete="off"
                                value="8">
                            <label class="btn btn-outline-dark worker-modal-btn" for="marcenaria">Marcenaria</label>

                            {{-- Demolição --}}
                            <input type="radio" class="btn-check" name="catOpt" id="demolicao" autocomplete="off"
                                value="2">
                            <label class="btn btn-outline-dark worker-modal-btn" for="demolicao">Demolição</label>

                            {{-- Empreiteira --}}
                            <input type="radio" class="btn-check" name="catOpt" id="empreiteira" autocomplete="off"
                                value="4">
                            <label class="btn btn-outline-dark worker-modal-btn" for="empreiteira">Empreiteira</label>
                        </div>

                        {{-- Col-dir --}}
                        <div style="display: flex; flex-flow:column wrap; gap:20px;">
                            {{-- Pedreiro --}}
                            <input type="radio" class="btn-check" name="catOpt" id="pedreiro"
                                autocomplete="off" value="9">
                            <label class="btn btn-outline-dark worker-modal-btn" for="pedreiro">Pedreiro</label>

                            {{-- Engenharia --}}
                            <input type="radio" class="btn-check" name="catOpt" id="engenharia"
                                autocomplete="off" value="6">
                            <label class="btn btn-outline-dark worker-modal-btn" for="engenharia">Engenharia</label>

                            {{-- Elétrica --}}
                            <input type="radio" class="btn-check" name="catOpt" id="eletrica"
                                autocomplete="off" value="3">
                            <label class="btn btn-outline-dark worker-modal-btn" for="eletrica">Eletricista</label>

                            {{-- Fretes --}}
                            <input type="radio" class="btn-check" name="catOpt" id="fretes"
                                autocomplete="off" value="7">
                            <label class="btn btn-outline-dark worker-modal-btn" for="fretes">Fretes</label>

                            {{-- Pintura --}}
                            <input type="radio" class="btn-check" name="catOpt" id="pintura"
                                autocomplete="off" value="10">
                            <label class="btn btn-outline-dark worker-modal-btn" for="pintura">Pintura</label>

                        </div>
                    </div>

                    <div class="d-grid gap-2">
                        <button class="btn btn-dark" type="submit" value="submit">Filtrar</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
