<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Salva Obra</title>
</head>

<body id="worker-registration-page" style="padding: 50px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" id="registration-input-card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            {{-- <form method="POST" action="{{ route('worker.store') }}"> --}}
                            @csrf
                            <h3 class="registration-form-title">Nova Conta</h3>

                            {{-- E-mail --}}
                            <div class="row mb-2 mt-5">
                                <label for="email"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('E-mail') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Password --}}
                            <div class="row mb-2">
                                <label for="password"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Senha') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Confirm-Password --}}
                            <div class="row mb-5">
                                <label for="password-confirm"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Confirmar senha') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                        name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>


                            {{-- Name --}}
                            <div class="row mb-2 mt-5">
                                <label for="name"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Nome') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Last Name < Hidden > --}}
                            <div class="row">
                                <div class="col-md-6">
                                    <input id="last_name" type="text"
                                        class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                                        value="-" required autocomplete="last_name" autofocus hidden>

                                    @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- CNPJ/CPF --}}
                            <div class="row mb-2">
                                <label for="document"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('CNPJ ou CPF') }}</label>
                                <div class="col-md-6">
                                    <input id="document" type="text"
                                        class="form-control @error('document') is-invalid @enderror" name="document"
                                        value="{{ old('document') }}" required autocomplete="document"
                                        pattern="/^([0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}|[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}\-?[0-9]{2})$/">
                                </div>
                            </div>



                            {{-- Phone --}}
                            <div class="row mb-2">
                                <label for="phone"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Telefone Principal') }}</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" required
                                        class="form-control @error('phone') is-invalid @enderror" name="phone"
                                        value="{{ old('phone') }}" autocomplete="phone" autofocus maxlength="20"
                                        pattern="^\(?[1-9]{2}\)? ?(?:[2-8]|9[0-9])[0-9]{3}\-?[0-9]{4}$">

                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Phone / work_phone --}}
                            <div class="row mb-5">
                                <label for="work_phone"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Telefone Secundário (opcional)') }}</label>

                                <div class="col-md-6">
                                    <input id="work_phone" type="text"
                                        class="form-control @error('work_phone') is-invalid @enderror"
                                        name="work_phone" value="-" autocomplete="work_phone" autofocus
                                        maxlength="20"
                                        pattern="^\(?[1-9]{2}\)? ?(?:[2-8]|9[0-9])[0-9]{3}\-?[0-9]{4}$">

                                    @error('work_phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            {{-- Adress inputs region --}}

                            {{-- Number --}}
                            <div class="row mb-3 mt-5">
                                <label for="number"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Número') }}</label>
                                <div class="col-md-6">
                                    <input id="number" type="number"
                                        class="form-control @error('number') is-invalid @enderror" name="number"
                                        value="{{ old('number') }}" required autocomplete="name" autofocus>

                                    @error('number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Street --}}
                            <div class="row mb-3 mt-3">
                                <label for="street"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Rua') }}</label>
                                <div class="col-md-6">
                                    <input id="street" type="text"
                                        class="form-control @error('street') is-invalid @enderror" name="street"
                                        value="{{ old('street') }}" required autocomplete="name" autofocus>

                                    @error('street')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Complemento --}}
                            <div class="row mb-3 mt-3">
                                <label for="complement"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Complemento') }}</label>
                                <div class="col-md-6">
                                    <input id="complement" type="text"
                                        class="form-control @error('complement') is-invalid @enderror"
                                        name="complement" value="{{ old('complement') }}" required
                                        autocomplete="name" autofocus>

                                    @error('complement')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Bairro --}}
                            <div class="row mb-3 mt-3">
                                <label for="neighborhood"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Bairro') }}</label>
                                <div class="col-md-6">
                                    <input id="neighborhood" type="text"
                                        class="form-control @error('neighborhood') is-invalid @enderror"
                                        name="neighborhood" value="{{ old('neighborhood') }}" required
                                        autocomplete="name" autofocus>

                                    @error('neighborhood')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Cidade --}}
                            <div class="row mb-3 mt-3">
                                <label for="city"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Cidade') }}</label>
                                <div class="col-md-6">
                                    <input id="city" type="text"
                                        class="form-control @error('city') is-invalid @enderror" name="city"
                                        value="{{ old('city') }}" required autocomplete="name" autofocus>

                                    @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Estado --}}
                            {{-- Original --}}
                            {{-- <div class="row mb-3 mt-3">
                                <label for="uf"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Estado') }}</label>
                                <div class="col-md-6">
                                    <input id="uf" type="text"
                                        class="form-control @error('uf') is-invalid @enderror" name="uf"
                                        value="{{ old('uf') }}" required autocomplete="name" autofocus>

                                    @error('uf')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> --}}

                            <div class="row mb-3 mt-3">
                                <label for="uf"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Estado') }}</label>
                                <div class="col-md-6">
                                    <select id="uf" type="text"
                                        class="form-control @error('uf') is-invalid @enderror" name="uf"
                                        value="{{ old('uf') }}" required autocomplete="name" autofocus>
                                        <option value="AC">Acre</option>
                                        <option value="AL">Alagoas</option>
                                        <option value="AP">Amapá</option>
                                        <option value="AM">Amazonas</option>
                                        <option value="BA">Bahia</option>
                                        <option value="CE">Ceará</option>
                                        <option value="DF">Distrito Federal</option>
                                        <option value="ES">Espírito Santo</option>
                                        <option value="GO">Goías</option>
                                        <option value="MA">Maranhão</option>
                                        <option value="MT">Mato Grosso</option>
                                        <option value="MS">Mato Grosso do Sul</option>
                                        <option value="MG">Minas Gerais</option>
                                        <option value="PA">Pará</option>
                                        <option value="PB">Paraíba</option>
                                        <option value="PR">Paraná</option>
                                        <option value="PE">Pernambuco</option>
                                        <option value="PI">Piauí</option>
                                        <option value="RJ">Rio de Janeiro</option>
                                        <option value="RN">Rio Grande do Norte</option>
                                        <option value="RS">Rio Grande do Sul</option>
                                        <option value="RO">Rondônia</option>
                                        <option value="RR">Roraima</option>
                                        <option value="SC">Santa Catarina</option>
                                        <option value="SP">São Paulo</option>
                                        <option value="SE">Sergipe</option>
                                        <option value="TO">Tocantins</option>
                                    </select>
                                    @error('uf')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- CEP --}}
                            <div class="row mb-3 mt-3">
                                <label for="CEP"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('CEP') }}</label>
                                <div class="col-md-6">
                                    <input id="CEP" type="text"
                                        class="form-control @error('CEP') is-invalid @enderror" name="CEP"
                                        value="{{ old('CEP') }}" required autocomplete="name" autofocus>

                                    @error('CEP')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Description --}}
                            <div class="row mb-3 mt-3">
                                <label for="description"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Descrição Profissional') }}</label>
                                <div class="col-lg-6">
                                    <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror"
                                        name="description" value="{{ old('description') }}" required autocomplete="description" autofocus
                                        style="height: 100px;"></textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- Work Categories --}}
                            <div class="row mb-3 mt-3">
                                <label for="workCategories"
                                    class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Categorias') }}
                                </label>
                                <div class="col-lg-6">
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk1" name="work_cat[]" value="1">
                                            <span>Arquitetura</span>
                                        </label>
                                    </div>
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk2" name="work_cat[]" value="2">
                                            <span>Demolição</span>
                                        </label>
                                    </div>
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk3" name="work_cat[]" value="3">
                                            <span>Eletricista</span>
                                        </label>
                                    </div>
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk3" name="work_cat[]" value="4">
                                            <span>Empreiteira</span>
                                        </label>
                                    </div>
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk3" name="work_cat[]" value="5">
                                            <span>Encanamento</span>
                                        </label>
                                    </div>
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk3" name="work_cat[]" value="6">
                                            <span>Engenharia</span>
                                        </label>
                                    </div>
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk3" name="work_cat[]" value="7">
                                            <span>Frete</span>
                                        </label>
                                    </div>
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk3" name="work_cat[]" value="8">
                                            <span>Marcenaria</span>
                                        </label>
                                    </div>
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk3" name="work_cat[]" value="9">
                                            <span>Pedreiro</span>
                                        </label>
                                    </div>
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk3" name="work_cat[]" value="10">
                                            <span>Pintura</span>
                                        </label>
                                    </div>
                                    <div class="cat action">
                                        <label>
                                            <input type="checkbox" id="wk3" name="work_cat[]" value="11">
                                            <span>Servente</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                    </div>

                    {{-- Categories Options --}}


                    {{-- Form Buttons --}}
                    <div class="row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-dark" style="width: 80%; min-width: fit-content;">
                                {{ __('Finalizar') }}
                            </button>
                        </div>
                    </div>
                    </form>

                    <div class="row mb-2 mt-2">
                        <div class="col-md-6 offset-md-4">
                            <a href="{{ route('home') }}">
                                <button type="btn" class="btn btn-outline-dark"
                                    style="width: 80%; min-width: fit-content;">
                                    {{ __('Cancelar') }}
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    {{-- Bootstrap.js --}}
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
</body>

</html>
