@extends('templates/generalPagesTemplate')

@section('l-head-title')
    <p>Perfil</p>
@endsection

@section('GPT-container-l')
    <div class="GPT-container-l-a-container">
        <div class="GPT-container-l-avatar-area">
            @if (isset($worker->user->image?->src))
                <img class="profile-avatar" src="{{ $worker->user->image?->src }}" alt="{{ $worker->user->name }}"
                    srcset="">
            @else
                <img class="profile-avatar" src="https://picsum.photos/200/300" alt="Avatar">
            @endif
            <p class="profile-worker-name">{{ $worker->user->name }}</p>
            <p class="profile-worker-location">
                {{ $worker->user->address()?->city }} / {{ $worker->user->address()?->uf }}
            </p>
        </div>

        <div class="GPT-container-l-contact">
            <div style="margin-left: 15px">
                <p class="profile-l-title">Contato</p>
                <div
                    style="border:1px solid #DDDDDD; padding: 16px 14px; border-radius: 5px; width: 227px; overflow-x: scroll;">
                    <div style="display: flex; flex-flow: row nowrap;">
                        <img src="{{ asset('images/icons/email.svg') }}" alt="">
                        <span class="profile-l-data">{{ $worker->user->email }}</span>
                    </div>
                    <hr>
                    <div>
                        <img src="{{ asset('images/icons/phone.svg') }}" alt="">
                        <span class="profile-l-data">{{ $worker->work_phone }}</span>
                    </div>
                    <hr>
                    <div>
                        <img src="{{ asset('images/icons/phone.svg') }}" alt="">
                        <span class="profile-l-data">{{ $worker->user->phone }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="GPT-container-l-contact">
            <div style="margin-left: 15px">
                <p class="profile-l-title">Endereço</p>
                <div
                    style="border:1px solid #DDDDDD; padding: 16px 14px; border-radius: 5px; width: 227px;overflow-x: scroll;">
                    <div>
                        <img src="{{ asset('images/icons/cep.svg') }}" alt="">
                        <span class="profile-l-data">CEP :
                            {{ substr_replace($worker->user->address()?->CEP, '-', 5, 0) }}</span>
                    </div>
                    <hr>
                    <div>
                        <img src="{{ asset('images/icons/map.svg') }}" alt="">
                        <span class="profile-l-data">End : {{ $worker->user->address()?->street }}</span>
                    </div>
                    <hr>
                    <div>
                        <img src="{{ asset('images/icons/bairro.svg') }}" alt="">
                        <span class="profile-l-data"> Bairro : {{ $worker->user->address()?->neighborhood }}</span>
                    </div>
                </div>
            </div>
        </div>

        <section class="workerCard-footer-score" style="margin-top: 40px;">
            <p class="workerCard-score-value" style="font-size: 50px !important; margin-bottom: -17px;">{{ $worker->rate }}
            </p>
            <p class="workerCard-score-nota" style="font-size: 30px !important;">Nota</p>
        </section>
    </div>
@endsection

@section('GPT-container-r')
    <div class="profile-data-central-container">
        <div class="profile-data-content">
            <img src="https://picsum.photos/1270/260" alt="" class="profile-content-img-header">
            <section class="profile-data-badges">
                @foreach ($worker->categories as $cat)
                    <x-category-badge :category="$cat->name" :hasName=true :profileText=true />
                @endforeach
            </section>
            <section class="profile-data-description">
                <p class="profile-l-title">Descrição</p>
                <div class="profile-description-box">
                    {{$worker->description}}
                </div>
            </section>
        </div>
    </div>
@endsection

{{-- Old implementation --}}

{{-- @extends('layouts.user')

@section('content')c:\Users\rafan\Downloads\user_avatar_placeholder.png
    <img src="{{ $worker->user->image?->src }}" alt="Imagem do carinha" srcset="">
    {{ $worker->user->name }}

    {{ $worker->user->address()?->city  }} / {{ $worker->user->address()?->uf }}
    <hr>
    {{ $worker->user->works()->count() }} | {{ $worker->likes }}
    <hr>
    @foreach ($worker->categories as $category)
        {{$category->id}}
        {{ $category->name }}
    @endforeach

    <hr>
    Contatos
    {{ $worker->worker_phone }}
    {{ $worker->user->email }}
    Endereço
    {{ $worker->user->address()?->CEP }}
    {{ $worker->user->address()?->street }}
    {{ $worker->user->address()?->number }}
    {{ $worker->user->address()?->neighborhood }}

    <hr>
    {{ $worker->description }}
@endsection --}}
