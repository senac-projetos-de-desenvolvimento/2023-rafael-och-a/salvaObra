<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Salva Obra</title>
</head>

<body id="HOME">
    <div id="GPT-center">
        <nav id="GPT-nav">
            <div class="GPT-nav-container">
                <a href="{{ route('home') }}">
                    <img src="{{ url('images/icons/navTitle.svg') }}" alt="" style="width: 84px;">
                </a>
                <a href="#">
                    <span style="margin-left: 45px">Quem somos</span>
                </a>
                <a href="#">
                    <span>Benefícios</span>
                </a>
            </div>
            <div class="GPT-nav-container">
                @yield('worker-searchBar')
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Entrar') }}</a>
                    @endif
                @else
                    <a href="#">
                        <img src="{{ url('images/icons/bell.svg') }}" alt="">
                    </a>
                    {{-- ! --}}
                    <div class="dropdown">
                        <a class="btn btn-outline-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            {{ Auth::user()->name }}
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li>
                                <a class="dropdown-item" href="#"><a class="dropdown-item"
                                        href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                            </li>
                        </ul>
                    </div>
                @endguest
            </div>
        </nav>

        <div id="GPT-container" style="height: 75vh;">
            {{-- Usuário Deslogado --}}
            @guest
                <div class="home-container-r"
                    style="text-align : center; display: flex; flex-flow: column nowrap; align-items: center;">
                    <h1 class="home-container-guest-h1">Bem vindo</h1>
                    <h3 class="home-container-guest-h3">
                        Encontre profissionais qualificados, ofereça seus serviços,<br> é hora de salvar uma obra!
                    </h3>
                    <div style="display: flex; flex-flow: column nowrap; gap:15px; margin-top: 5%;">
                        <a href="{{ route('worker.index') }}">
                            <button class="btn btn-dark" style="height: 54px; width: 550px; font-weight: 600;">Ver profissionais</button>
                        </a>
                        <a href="{{ route('accTypeSel') }}">
                            {{-- <a href="{{ route('register') }}"> --}}
                            <button class="btn btn-outline-dark" style="height: 54px; width: 550px; font-weight: 600;">Nova conta</button>
                        </a>
                    </div>
                </div>

                {{-- Usuário Logado --}}
            @else
                <div class="home-container-l">
                    <h3>Bem vindo,</h3>
                    <h1>{{ Auth::user()->name }}</h1>
                </div>
                <div class="home-container-r home-auth-r-container">
                    <a href="{{ route('worker.index') }}" style="text-decoration: none;">
                        <button class="home-auth-buttons">
                            <img src="{{ url('images/icons/icone_prestadores_home.svg') }}" alt="">
                            <div>
                                <p class="home-auth-buttons-titlte">Prestadores</p>
                                <p class="home-auth-buttons-desc">Contrate serviços de profissionais qualificados</p>
                            </div>
                        </button>
                    </a>
                    <a href="#" style="text-decoration: none;">
                        <button class="home-auth-buttons">
                            <img src="{{ url('images/icons/icone_servicos_home.svg') }}" alt="">
                            <div>
                                <p class="home-auth-buttons-titlte">Meus serviços</p>
                                <p class="home-auth-buttons-desc">Acompanhe os serviços que você contratou, ou está
                                    prestando</p>
                            </div>
                        </button>
                    </a>
                    <a href="{{ route('user.profile') }}" style="text-decoration: none;">
                        <button class="home-auth-buttons">
                            <img src="{{ url('images/icons/icone_perfil_home.svg') }}" alt="">
                            <div>
                                <p class="home-auth-buttons-titlte">Perfil</p>
                                <p class="home-auth-buttons-desc">Veja como seu perfil está sendo visto por outros usuários.
                                </p>
                            </div>
                        </button>
                    </a>
                </div>
            @endguest
        </div>

    </div>
    {{-- Bootstrap.js --}}
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
</body>

</html>
