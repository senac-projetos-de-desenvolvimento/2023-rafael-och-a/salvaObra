@extends('layouts.user')

@section('content')
{{ $user->name }}

serviços
{{ $user->services->count() }}
Favoritos:
{{ $user->likes->count() }}
trabalhados:
{{ $user->works->count() }}

<h1>Update perfil</h1>
<form action="{{ route('user.update',$user->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    <h1>Cadastro de usuários</h1>
    <div class="form-group">
      <label for="nome">Nome</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" placeholder="Nome do usuário" required>
    </div>
    <div class="form-group">
      <label for="sobrenome">Sobrenome</label>
      <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $user->last_name }}" placeholder="Sobrenome do usuário" required>
    </div>
    <div class="form-group">
      <label for="email">E-mail</label>
      <input type="email" class="form-control" value="{{ $user->email }}" placeholder="E-mail do usuário" readonly>
    </div>
    <div class="form-group">
      <label for="telefone">Telefone</label>
      <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}" placeholder="Telefone do usuário" >
    </div>
    <div class="form-group">
      <label for="perfil">Perfil</label>
      <input type="file" accept="image/jpeg, image/bmp, image/png" class="form-control" id="image" name="image" placeholder="perfil do usuário">
    </div>
    <button type="submit" class="btn btn-primary">Cadastrar</button>
</form>
@endsection
