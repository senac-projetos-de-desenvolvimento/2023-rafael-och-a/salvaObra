@extends('layouts.user')

@section("content")
    <img src="{{ $user->image->src }}" alt="Imagem do carinha" srcset="">
    {{ $worker->user->name }}
    {{ $worker->user->address->city  }} / {{ $worker->user->address->uf }}
    <hr>
    {{ $worker->user->works()->count() }} | {{ $worker->likes }}
    <hr>
    {{ $worker->categories()->count() }}
@endsection
