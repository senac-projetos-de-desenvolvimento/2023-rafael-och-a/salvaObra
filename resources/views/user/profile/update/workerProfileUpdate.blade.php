@extends('templates/profileUpdateTemplate')

@section('GPT-container-r')
    <div class="col-md-8">
        <div class="card" id="registration-input-card">
            <div class="card-body">
                <form method="POST" action="{{ route('user.update', Auth::user()->id) }}" enctype="multipart/form-data">
                    @csrf
                    <h3 class="registration-form-title">Atualizar dados</h3>
                    {{-- Name --}}
                    <div class="row mb-3 mt-5">
                        <label for="name"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Nome') }}</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                name="name" value="{{ Auth::user()->name }}" required autocomplete="name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- Last Name --}}
                    <div class="row mb-3">
                        <label for="last_name"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Sobrenome') }}</label>

                        <div class="col-md-6">
                            <input id="last_name" type="text"
                                class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                                value="{{ Auth::user()->last_name }}" required autocomplete="last_name" autofocus>

                            @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- Phone --}}
                    <div class="row mb-5">
                        <label for="phone"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Telefone') }}</label>

                        <div class="col-md-6">
                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror"
                                name="phone" value="{{ Auth::user()->phone }}" autocomplete="phone" autofocus
                                maxlength="20" pattern="^\(?[1-9]{2}\)? ?(?:[2-8]|9[0-9])[0-9]{3}\-?[0-9]{4}$">

                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- Phone / work_phone --}}
                    <div class="row mb-5">
                        <label for="work_phone"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Telefone Secundário (opcional)') }}</label>

                        <div class="col-md-6">
                            <input id="work_phone" type="text"
                                class="form-control @error('work_phone') is-invalid @enderror" name="work_phone"
                                value="{{ Auth::user()->worker->work_phone }}" autocomplete="work_phone" autofocus
                                maxlength="20" pattern="^\(?[1-9]{2}\)? ?(?:[2-8]|9[0-9])[0-9]{3}\-?[0-9]{4}$">

                            @error('work_phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- Number --}}
                    <div class="row mb-3 mt-5">
                        <label for="number"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Número') }}</label>
                        <div class="col-md-6">
                            <input id="number" type="number" class="form-control @error('number') is-invalid @enderror"
                                name="number" value="{{ Auth::user()->address()?->number }}" required autocomplete="name"
                                autofocus>

                            @error('number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- Street --}}
                    <div class="row mb-3 mt-3">
                        <label for="street"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Rua') }}</label>
                        <div class="col-md-6">
                            <input id="street" type="text" class="form-control @error('street') is-invalid @enderror"
                                name="street" value="{{ Auth::user()->address()?->street }}" required autocomplete="name"
                                autofocus>

                            @error('street')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- Complemento --}}
                    <div class="row mb-3 mt-3">
                        <label for="complement"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Complemento') }}</label>
                        <div class="col-md-6">
                            <input id="complement" type="text"
                                class="form-control @error('complement') is-invalid @enderror" name="complement"
                                value="{{ Auth::user()->address()?->complement }}" required autocomplete="name" autofocus>

                            @error('complement')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                    {{-- Bairro --}}
                    <div class="row mb-3 mt-3">
                        <label for="neighborhood"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Bairro') }}</label>
                        <div class="col-md-6">
                            <input id="neighborhood" type="text"
                                class="form-control @error('neighborhood') is-invalid @enderror" name="neighborhood"
                                value="{{ Auth::user()->address()?->neighborhood }}" required autocomplete="name"
                                autofocus>

                            @error('neighborhood')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- Cidade --}}
                    <div class="row mb-3 mt-3">
                        <label for="city"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Cidade') }}</label>
                        <div class="col-md-6">
                            <input id="city" type="text" class="form-control @error('city') is-invalid @enderror"
                                name="city" value="{{ Auth::user()->address()?->city }}" required autocomplete="name"
                                autofocus>

                            @error('city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3 mt-3">
                        <label for="uf"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Estado') }}</label>
                        <div class="col-md-6">
                            <select id="uf" type="text" class="form-control @error('uf') is-invalid @enderror"
                                name="uf" value="{{ Auth::user()->address()?->uf }}" required autocomplete="name"
                                autofocus>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espírito Santo</option>
                                <option value="GO">Goías</option>
                                <option value="MA">Maranhão</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>
                            </select>
                            @error('uf')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- CEP --}}
                    <div class="row mb-3 mt-3">
                        <label for="CEP"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('CEP') }}</label>
                        <div class="col-md-6">
                            <input id="CEP" type="text" class="form-control @error('CEP') is-invalid @enderror"
                                name="CEP" value="{{ Auth::user()->address()?->CEP }}" required autocomplete="name"
                                autofocus>

                            @error('CEP')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- Description --}}
                    <div class="row mb-3 mt-3">
                        <label for="description"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Descrição Profissional') }}</label>
                        <div class="col-lg-6">
                            <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror"
                                name="description" value="{{ Auth::user()->worker->description }}" required autocomplete="description" autofocus
                                style="height: 100px;">{{ Auth::user()->worker->description }}</textarea>

                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- Work Categories --}}
                    <div class="row mb-3 mt-3">
                        <label for="workCategories"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Categorias') }}
                        </label>
                        <div class="col-lg-6">
                            <div class="cat action">
                                <label>
                                    @if (in_array(1, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="1" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="1">
                                    @endif
                                    <span>Arquitetura</span>
                                </label>
                            </div>
                            <div class="cat action">
                                <label>
                                    @if (in_array(2, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="2" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="2">
                                    @endif
                                    <span>Demolição</span>
                                </label>
                            </div>
                            <div class="cat action">
                                <label>
                                    @if (in_array(3, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="3" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="3">
                                    @endif
                                    <span>Eletricista</span>
                                </label>
                            </div>
                            <div class="cat action">
                                <label>
                                    @if (in_array(4, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="4" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="4">
                                    @endif
                                    <span>Empreiteira</span>
                                </label>
                            </div>
                            <div class="cat action">
                                <label>
                                    @if (in_array(5, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="5" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="5">
                                    @endif
                                    <span>Encanamento</span>
                                </label>
                            </div>
                            <div class="cat action">
                                <label>
                                    @if (in_array(6, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="6" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="6">
                                    @endif
                                    <span>Engenharia</span>
                                </label>
                            </div>
                            <div class="cat action">
                                <label>
                                    @if (in_array(7, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="7" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="7">
                                    @endif
                                    <span>Frete</span>
                                </label>
                            </div>
                            <div class="cat action">
                                <label>
                                    @if (in_array(8, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="8" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="8">
                                    @endif
                                    <span>Marcenaria</span>
                                </label>
                            </div>
                            <div class="cat action">
                                <label>
                                    @if (in_array(9, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="9" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="9">
                                    @endif
                                    <span>Pedreiro</span>
                                </label>
                            </div>
                            <div class="cat action">
                                <label>
                                    @if (in_array(10, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="10" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="10">
                                    @endif
                                    <span>Pintura</span>
                                </label>
                            </div>
                            <div class="cat action">
                                <label>
                                    @if (in_array(11, $wCat))
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="11" checked>
                                    @else
                                        <input type="checkbox" id="wk1" name="work_cat[]" value="11">
                                    @endif
                                    <span>Servente</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    {{-- AVATAR --}}
                    <div class="row mb-3 mt-3">
                        <label for="avatar"
                            class="col-md-4 col-form-label text-md-end registration-form-label">{{ __('Avatar') }}</label>
                        <div class="col-md-6">
                            <input id="avatar" type="file"
                                class="form-control @error('avatar') is-invalid @enderror" name="avatar" value=""
                                autocomplete="name" autofocus>

                            @error('avatar')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-dark" style="width: 80%; min-width: fit-content;">
                                {{ __('Finalizar') }}
                            </button>
                        </div>
                    </div>
                </form>

                <div class="row mb-2 mt-2">
                    <div class="col-md-6 offset-md-4">
                        <a href="{{ route('home') }}">
                            <button type="btn" class="btn btn-outline-dark"
                                style="width: 80%; min-width: fit-content;">
                                {{ __('Cancelar') }}
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
