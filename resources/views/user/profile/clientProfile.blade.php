<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Salva Obra</title>
</head>

<body id="GPT">
    <div id="GPT-center">
        <nav id="GPT-nav">
            <div class="GPT-nav-container">
                <a href="{{ route('home') }}">
                    <img src="{{ url('images/icons/navTitle.svg') }}" alt="" style="width: 84px;">
                </a>
                <a href="#">
                    <span style="margin-left: 45px">Quem somos</span>
                </a>
                <a href="#">
                    <span>Benefícios</span>
                </a>
            </div>
            <div class="GPT-nav-container">
                @yield('worker-searchBar')
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Entrar') }}</a>
                    @endif
                @else
                    <a href="#">
                        <img src="{{ url('images/icons/bell.svg') }}" alt="">
                    </a>
                    {{-- ! --}}
                    <div class="dropdown">
                        <a class="btn btn-outline-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            {{ Auth::user()->name }}
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li>
                                <a class="dropdown-item" href="#"><a class="dropdown-item"
                                        href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                            </li>
                        </ul>
                    </div>
                @endguest
            </div>
        </nav>
        <div id="GPT-container" style="justify-content: center;">
            <div id="client-profile-container">
                <header class="GPT-container-l-head">Seu perfil</header>
                <div style="display: flex; flex-flow: column; align-items: center;">
                    <div class="GPT-container-l-avatar-area" style="margin-top: 20px;">
                        @if ($userImg->src != '-')
                            <img class="profile-avatar" src="/storage/{{ $userImg->src }}"
                                alt="{{ Auth::user()->name }}" srcset="">
                        @else
                            <img class="profile-avatar" src="https://picsum.photos/200/300" alt="Avatar">
                        @endif
                        <p class="profile-worker-name">{{ Auth::user()->name }}</p>
                        <p class="profile-worker-location">
                            {{ Auth::user()->address()?->city }} / {{ Auth::user()->address()?->uf }}
                        </p>
                    </div>

                    {{-- Contato --}}
                    <div class="GPT-container-l-contact">
                        <div style="margin-top: 10px;">
                            <p class="profile-l-title">Contato</p>
                            <div
                                style="border:1px solid #DDDDDD; padding: 16px 14px; border-radius: 5px; width: 227px; overflow-x: scroll;">
                                <div style="display: flex; flex-flow: row nowrap;">
                                    <img src="{{ asset('images/icons/email.svg') }}" alt="">
                                    <span class="profile-l-data">{{ Auth::user()->email }}</span>
                                </div>
                                <hr>
                                @if (isset(Auth::user()->worker))
                                    <div>
                                        <img src="{{ asset('images/icons/phone.svg') }}" alt="">
                                        <span class="profile-l-data">{{ Auth::user()->worker->work_phone }}</span>
                                    </div>
                                    <hr>
                                @endif
                                <div>
                                    <img src="{{ asset('images/icons/phone.svg') }}" alt="">
                                    <span class="profile-l-data">{{ Auth::user()->phone }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Endereço --}}
                    <div class="GPT-container-l-contact">
                        <div style="margin-top: 15px;">
                            <p class="profile-l-title">Endereço</p>
                            <div
                                style="border:1px solid #DDDDDD; padding: 16px 14px; border-radius: 5px; width: 227px;overflow-x: scroll;">
                                <div>
                                    <img src="{{ asset('images/icons/cep.svg') }}" alt="">
                                    <span class="profile-l-data">CEP :
                                        {{ substr_replace(Auth::user()->address()?->CEP, '-', 5, 0) }}</span>
                                </div>
                                <hr>
                                <div>
                                    <img src="{{ asset('images/icons/map.svg') }}" alt="">
                                    <span class="profile-l-data">End : {{ Auth::user()->address()?->street }}</span>
                                </div>
                                <hr>
                                <div>
                                    <img src="{{ asset('images/icons/bairro.svg') }}" alt="">
                                    <span class="profile-l-data"> Bairro :
                                        {{ Auth::user()->address()?->neighborhood }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href={{route('user.edit', Auth::user()->id)}} class="updateProfileLink" style="margin-left:15px;" >
                    Atualizar perfil
                </a>
            </div>
        </div>

    </div>
    {{-- Bootstrap.js --}}
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
</body>

</html>
