@extends('templates/generalPagesTemplate')

@section('l-head-title')
    <p>Seu Perfil</p>
@endsection

@section('GPT-container-l')
    <div class="GPT-container-l-a-container">
        <div class="GPT-container-l-avatar-area">
            @if ($userImg->src != '-')
                <img class="profile-avatar" src="/storage/{{ $userImg->src }}" alt="{{ Auth::user()->name }}" srcset="">
            @else
                <img class="profile-avatar" src="https://picsum.photos/200/300" alt="Avatar">
            @endif
            <p class="profile-worker-name">{{ Auth::user()->name }}</p>
            <p class="profile-worker-location">
                {{ Auth::user()->address()?->city }} / {{ Auth::user()->address()?->uf }}
            </p>
        </div>

        <div class="GPT-container-l-contact">
            <div style="margin-left: 15px">
                <p class="profile-l-title">Contato</p>
                <div
                    style="border:1px solid #DDDDDD; padding: 16px 14px; border-radius: 5px; width: 227px; overflow-x: scroll;">
                    <div style="display: flex; flex-flow: row nowrap;">
                        <img src="{{ asset('images/icons/email.svg') }}" alt="">
                        <span class="profile-l-data">{{ Auth::user()->email }}</span>
                    </div>
                    <hr>
                    @if (isset(Auth::user()->worker))
                        <div>
                            <img src="{{ asset('images/icons/phone.svg') }}" alt="">
                            <span class="profile-l-data">{{ Auth::user()->worker->work_phone }}</span>
                        </div>
                        <hr>
                    @endif
                    <div>
                        <img src="{{ asset('images/icons/phone.svg') }}" alt="">
                        <span class="profile-l-data">{{ Auth::user()->phone }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="GPT-container-l-contact">
            <div style="margin-left: 15px">
                <p class="profile-l-title">Endereço</p>
                <div
                    style="border:1px solid #DDDDDD; padding: 16px 14px; border-radius: 5px; width: 227px;overflow-x: scroll;">
                    <div>
                        <img src="{{ asset('images/icons/cep.svg') }}" alt="">
                        <span class="profile-l-data">CEP :
                            {{ substr_replace(Auth::user()->address()?->CEP, '-', 5, 0) }}</span>
                    </div>
                    <hr>
                    <div>
                        <img src="{{ asset('images/icons/map.svg') }}" alt="">
                        <span class="profile-l-data">End : {{ Auth::user()->address()?->street }}</span>
                    </div>
                    <hr>
                    <div>
                        <img src="{{ asset('images/icons/bairro.svg') }}" alt="">
                        <span class="profile-l-data"> Bairro : {{ Auth::user()->address()?->neighborhood }}</span>
                    </div>
                </div>
            </div>
        </div>
        @if (isset(Auth::user()->worker))
            <section class="workerCard-footer-score" style="margin-top: 40px;">
                <p class="workerCard-score-value" style="font-size: 50px !important; margin-bottom: -17px;">
                    {{ Auth::user()->worker->rate }}
                </p>
                <p class="workerCard-score-nota" style="font-size: 30px !important;">Nota</p>
            </section>
        @endif
    </div>
@endsection

@section('GPT-container-r')
    <div class="profile-data-central-container">
        <div class="profile-data-content">
            @if (isset(Auth::user()->worker))
                <a href={{ route('user.edit', Auth::user()->id) }} class="updateProfileLink">Atualizar perfil</a>
                <img src="https://picsum.photos/1270/260" alt="" class="profile-content-img-header">
                <section class="profile-data-badges">
                    @foreach (Auth::user()->worker->categories as $cat)
                        <x-category-badge :category="$cat->name" :hasName=true :profileText=true />
                    @endforeach
                </section>
                <section class="profile-data-description">
                    <p class="profile-l-title">Descrição</p>
                    <div class="profile-description-box">
                        {{ Auth::user()->worker->description }}
                    </div>
                </section>
            @endif
        </div>
    </div>
@endsection
