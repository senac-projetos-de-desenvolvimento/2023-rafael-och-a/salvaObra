<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Salva Obra</title>
</head>

{{-- Mesmo que o GPT, mas não contém o painel esquerdo --}}

<body id="GPT">
    <div id="GPT-center">
        <nav id="GPT-nav">
            <div class="GPT-nav-container">
                <a href="{{route('home')}}">
                    <img src="{{ url('images/icons/navTitle.svg') }}" alt="" style="width: 84px;">
                </a>
                <a href="#" class="left-nav-content">
                    <span style="margin-left: 45px">Quem somos</span>
                </a>
                <a href="#" class="left-nav-content">
                    <span>Benefícios</span>
                </a>
            </div>
            <div class="GPT-nav-container GPT-nav-container-smallMargin ">
                @yield('worker-searchBar')
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Entrar') }}</a>
                    @endif
                @else
                    <a href="#">
                        <img src="{{ url('images/icons/bell.svg') }}" alt="">
                    </a>
                    {{-- ! --}}
                    <div class="dropdown">
                        <a class="btn btn-outline-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            {{ Auth::user()->name }}
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li><a class="dropdown-item" href="#"><a class="dropdown-item"
                                        href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                            </li>
                        </ul>
                    </div>
                @endguest
            </div>
        </nav>
        <div id="GPT-container">
            <section id="profile-form">
                @yield('GPT-container-r')
            </section>
        </div>
    </div>
    {{-- Bootstrap.js --}}
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
</body>

</html>
