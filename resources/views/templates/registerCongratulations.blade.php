<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Salva Obra</title>
</head>

<body id="GPT">
    <div style="display: flex; flex-flow: column nowrap; align-items: center;">
        @if (isset(Auth::user()->worker))
            <img src="{{ url('images/reg_finish_worker.png') }}" alt=""
                style="width: auto; max-width: 33vw; margin-bottom: 30px;">
        @else
        <img src="{{ url('images/reg_finish_client.png') }}" alt=""
            style="width: auto; max-width: 33vw; margin-bottom: 30px;">
        @endif
        <h3 class="regFinishTitle">Cadastro Finalizado com sucesso !</h3>

        <a href="{{ route('home') }}">
            <button class="btn btn-dark regFinishBtn" style="width: 23vw; margin-top: 5px;">OK</button>
        </a>
    </div>
    {{-- Bootstrap.js --}}
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
</body>

</html>
