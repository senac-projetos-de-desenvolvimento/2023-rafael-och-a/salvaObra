@extends('layouts.admin')

@section('content')
{{ $user->name }}

<h1>Update perfil</h1>
<form action="{{ route('admin.user.update') }}" method="POST" enctype="multipart/form-data">
    <h1>Cadastro de usuários</h1>
    <input type="hidden" class="form-control" id="id" value="{{ $user->id }}" name="id" placeholder="ID do usuário">
    <div class="form-group">
      <label for="nome">Nome</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" placeholder="Nome do usuário">
    </div>
    <div class="form-group">
      <label for="sobrenome">Sobrenome</label>
      <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $user->last_name }}" placeholder="Sobrenome do usuário">
    </div>
    <div class="form-group">
      <label for="email">E-mail</label>
      <input type="email" class="form-control" value="{{ $user->email }}" placeholder="E-mail do usuário" readonly>
    </div>
    <div class="form-group">
      <label for="senha">Senha</label>
      <input type="password" class="form-control" id="password" name="password" value="**********" placeholder="Senha do usuário">
    </div>
    <div class="form-group">
      <label for="telefone">Telefone</label>
      <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}" placeholder="Telefone do usuário">
    </div>
    <div class="form-group">
      <label for="perfil">Perfil</label>
      <input type="text" class="form-control" id="image_id" name="image_id" placeholder="perfil do usuário">
    </div>
    <button type="submit" class="btn btn-primary">Cadastrar</button>
</form>
@endsection
