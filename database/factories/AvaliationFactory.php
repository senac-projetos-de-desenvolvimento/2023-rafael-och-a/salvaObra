<?php

namespace Database\Factories;

use App\Models\Image;
use App\Models\Service;
use App\Models\User;
use App\Models\Worker;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Avaliation>
 */
class AvaliationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id'=>User::inRandomOrder()->first()->id,
            'worker_id'=>Worker::inRandomOrder()->first()->id,
            'service_id'=>Service::inRandomOrder()->first()->id,
            'image_id'=>Image::inRandomOrder()->first()->id,
            'rate'=>random_int(0,5),
            'description'=>fake()->optional()->text(),
        ];
    }
}
