<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Address>
 */
class AddressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'street'=>fake('pt-br')->streetAddress(),
            'neighborhood'=>fake('pt-br')->text(10),
            'city'=>fake('pt-br')->city(),
            'uf'=>fake('pt-br')->randomElement(['RS','SC','SP','PR']),
            'number'=>random_int(1,4000),
            'cep'=>random_int(10000000,99999999),
            'complement'=>fake('pt-br')->optional(0.2)->text(random_int(5,15)),
            'user_id' => \App\Models\User::inRandomOrder()->first()->id,
        ];
    }
}
