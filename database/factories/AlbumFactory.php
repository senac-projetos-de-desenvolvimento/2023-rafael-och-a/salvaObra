<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Album>
 */
class AlbumFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name'=>fake('pt-br')->text(random_int(5,32)),
            'description'=>fake('pt-br')->optional(0.9)->text(random_int(5,200)),
            'user_id'=>User::inRandomOrder()->first()->id,
            'totalImages'=>random_int(0,30),
        ];
    }
}
