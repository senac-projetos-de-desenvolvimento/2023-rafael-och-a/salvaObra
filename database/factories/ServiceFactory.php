<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Budget;
use App\Models\Category;
use App\Models\User;
use App\Models\Worker;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Service>
 */
class ServiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'price' => $this->faker->randomFloat(2, 10, 500),
            'daysToComplete' => $this->faker->numberBetween(1, 30),
            'start_date' => $this->faker->date(),
            'user_id' => User::inRandomOrder()->first()->id,
            'worker_id' => Worker::inRandomOrder()->first()->id,
            'category_id' => Category::inRandomOrder()->first()->id,
            'address_id' => Address::inRandomOrder()->first()->id,
            'budget_id' => Budget::inRandomOrder()->first()->id,
        ];
    }
}
