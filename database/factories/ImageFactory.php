<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Image>
 */
class ImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'src'=> $this->faker->randomElement(['1.jpg','2.jpg','3.jpg','4.jpg']),
            'alt' => $this->faker->text(64),
            'user_id'=> User::inRandomOrder()->first()->id,
        ];
    }
}
