<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Budget;
use App\Models\Category;
use App\Models\User;
use App\Models\Worker;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Budget>
 */
class BudgetFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        //Random para ver se esse budget referencia outro
        $rand = Budget::all()->count()<=0?false:(random_int(0,10)>4?true:false);
        return [
            'name' => $this->faker->word,
            'userDescription' => $this->faker->text(200),
            'workerDescription' => $this->faker->text(200),
            'price' => $this->faker->randomFloat(2, 100, 5000),
            'daysToComplete' => $this->faker->numberBetween(1, 30),
            'start_date' => $this->faker->date(),
            'user_id' => User::inRandomOrder()->first()->id,
            'worker_id' => Worker::inRandomOrder()->first()->id,
            'category_id' => Category::inRandomOrder()->first()->id,
            'address_id' => Address::inRandomOrder()->first()->id,
            'budget_id' => $rand?Budget::inRandomOrder()->first()->id:null,
        ];
    }
}
