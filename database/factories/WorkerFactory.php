<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Worker>
 */
class WorkerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::whereDoesntHave('worker')->inRandomOrder()->first()->id;
        return [
            'work_phone' => fake()->unique()->phoneNumber(),
            'document' => fake()->unique()->randomElement([random_int(10000000000,99999999999), random_int(10000000000000,99999999999999)]),
            'description' => fake()->optional(0.9)->text(),
            'user_id' => $user,
            'rate'=> random_int(0,50)/10,
        ];
    }
}
