<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Address;
use App\Models\Album;
use App\Models\AlbumImage;
use App\Models\Avaliation;
use App\Models\Budget;
use App\Models\Category;
use App\Models\Image;
use App\Models\Like;
use App\Models\Service;
use App\Models\User;
use App\Models\Worker;
use App\Models\WorkerCategory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run(): void
    {
        //Criação de categorias
            $categories=[
                [
                    'name'=>'Arquitetura',
                    'icon'=>'arquitetura.svg',
                ],
                [
                    'name'=>'Demolicao',
                    'icon'=>'demolicao.svg',
                ],
                [
                    'name'=>'Eletricista',
                    'icon'=>'eletricista.svg',
                ],
                [
                    'name'=>'Empreiteira',
                    'icon'=>'empreiteira.svg',
                ],
                [
                    'name'=>'Encanamento',
                    'icon'=>'encanamento.svg',
                ],
                [
                    'name'=>'Engenharia',
                    'icon'=>'engenharia.svg',
                ],
                [
                    'name'=>'Frete',
                    'icon'=>'frete.svg',
                ],
                [
                    'name'=>'Marcenaria',
                    'icon'=>'marcenaria.svg',
                ],
                [
                    'name'=>'Pedreiro',
                    'icon'=>'pedreiro.svg',
                ],
                [
                    'name'=>'Pintura',
                    'icon'=>'pintura.svg',
                ],
                [
                    'name'=>'Servente',
                    'icon'=>'servente.svg',
                ],
                //FINALIZAR AS CATEGORIAS
            ];
            foreach ($categories as $category) {
                Category::factory()->create([
                    'name' => $category['name'],
                    'icon' => $category['icon'],
                ]);
            }
        //Fim categorias

        if(User::where('email','rafaelochoamello@gmail.com')->count()==0)
        User::factory()->create([
            'name' => 'Rafael',
            'last_name' => 'Ochoa Mello',
            'email' => 'rafaelochoamello@gmail.com',
            'admin_level' => '1',
        ]);

        //Criação de base de uso
            User::factory(50)->create();
            Image::factory(10)->create();
            Worker::factory(35)->create();
            Like::factory(45)->create();
            Album::factory(50)->create();
            AlbumImage::factory(50)->create();
            Address::factory(60)->create();
            Budget::factory(15)->create();
            Service::factory(5)->create();
            Avaliation::factory(50)->create();
            WorkerCategory::factory(60)->create();
        //Fim base
    }
}
