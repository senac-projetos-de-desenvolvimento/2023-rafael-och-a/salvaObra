<?php

namespace Database\Seeders;

use App\Models\Avaliation;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AvaliationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Avaliation::factory(5000)->create();
    }
}
