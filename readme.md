# API

A API implementada para este projeto se baseia em **MySQL**, Utilizando **Laravel**. 
O Front utiliza o Blade, uma engine nativa do próprio Laravel.

## Downloads necessários

Em primeiro lugar, devemos ter o node instalado na máquina

https://nodejs.org/en/download

Em segundo lugar, devemos instalar o Xampp, ele já contém Apache web server, MySQL database, PHP, e Perl  

https://www.apachefriends.org/pt_br/download.html

Também é necessário a instalação do composer, o que seria equivalente a um 'npm do Laravel'

https://getcomposer.org/download/


Podemos também (porém não diretamente necessário), instalar o MySQL em separado
porém em alguns casos, pode haver conflito de versões com aquela vinda do Xampp
(Não recomendado) 

https://dev.mysql.com/downloads/workbench/

## Instalação e configuração

Em primeiro lugar, é necessário clonar este repositório 

    git clone https://gitlab.com/senac-projetos-de-desenvolvimento/2023-rafael-och-a/salvaObra.git

Em seguida, devemos instalar nossos pacotes, para isto utilizamos

    composer install

Após, devemos ajustar nossas variáveis de ambiente, para isto, basta
    
    - Fazer uma cópia do arquivo '.env.example'
    - O renomar para somente '.env'

Com tudo instalado, necessitamos gerar uma key do projeto

    php artisan key:generate

Com estas etapas realizadas, agora será necessário criar um branco no nosso MySQL workbench com as seguintes configurações

    Nome do banco  :: salvaobra
    Senha do banco :: 201160
    nome do user   :: root

Com o banco criado e as dependências do projeto instaladas, nos basta apenas subir as **migrações** que geram as tebelas e rodar a **seed** que irá popular o banco.

Para rodar as **migrações** basta utilizar 

    php artisan migrate

Para popular a tabela de categorias com as **seeds**

    php artisan db:seed

Com todas estas etapas realizadas, nos basta agora apenas rodar nosso projeto. Para isto, basta apenas utilizar

    php artisan serve

