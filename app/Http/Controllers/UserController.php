<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function dashboard()
    {
        //Apenas durante os testes
        if(!Auth::check())
            Auth::loginUsingId(1);

        return view('user.dashboard',[
            'user'=> Auth::user(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */

    //Testar essa implementação.
    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);
        $image = Image::where('user_id', $user->id)->firstOrFail();

        if(isset($user)){
            $user->name = $request->name;
            $user->last_name = $request->last_name;
            $user->phone = $request->phone;
            $user->address()->number = $request->number;
            $user->address()->street = $request->street;
            $user->address()->complement = $request->complement;
            $user->address()->neighborhood = $request->neighborhood;
            $user->address()->city = $request->city;
            $user->address()->uf = $request->uf;
            $user->address()->CEP = $request->CEP;
            $user->address()->save();

            if(isset($request->avatar)){
                $oldPath = $image->src === '-' ? '-' : $image->src;       
                $path = $request->file("avatar")->store('images', 'public');
                $image->src = $path;
                $image->alt = $user->name;                
                $image->save();
                //Remove old image from storage
                if($oldPath !== '-'){
                    unlink(storage_path('app/public/'.$oldPath));
                }
            }   
           
            if($user->worker){
                $user->worker->description = $request->description;
                if(strlen($request->work_phone) > 0)
                    $user->worker->work_phone = $request->work_phone;
                else
                    $user->worker->work_phone = "-";
                
                $user->worker->categories()->sync($request->work_cat);

                $user->worker->save();
            }
            $user->save();
        }
        return redirect('/');
        // $request->validate($user->rulesUpdate);
        // $user->update($request->except('image'));
        // return redirect()->back();
    }

    public function profile(Request $request){
        $image = Image::where('user_id', Auth::user()->id)->firstOrFail();

        //Se user()->worker == null É cliente
        if(isset(Auth::user()->worker)){
            return view('user.profile.profile', ['userImg'=>$image]);        
        }
        return view('user/profile/clientProfile', ['userImg'=>$image]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //Se user()->worker == null É cliente
        if(isset(Auth::user()->worker)){
            $wCat = $this->makeCategoriesArray();
            // return dd($wCat);
            return view('user/profile/update/workerProfileUpdate', ['wCat'=>$wCat]);
        }
        return view('user/profile/update/clientProfileUpdate');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    private function makeCategoriesArray(){
        $wCat = [];
        foreach(Auth::user()->worker->categories as $cat){
            array_push($wCat, $cat->id);
        }
        return $wCat;
    }
}


