<?php

namespace App\Http\Controllers;

use App\Models\Avaliation;
use App\Models\Worker;
use Illuminate\Http\Request;

class WorkerController extends Controller
{
    public function index(Request $request){
        $search = $request->search??'';
        
        $workers = Worker::orderBy('rate','desc')->distinct();
   
        if($request->has('search')){
            $workers = $workers->join('users','workers.user_id','users.id')
            ->join('worker_categories','workers.id','worker_categories.worker_id')
            ->select('workers.*')
            ->where(function ($query) use ($request){
                $query->where('users.name','LIKE','%'.$request->search.'%')
                ->orwhere('workers.description','LIKE','%'.$request->search.'%');
            })->distinct();
        }

        elseif($request->has('catOpt')){
            $workers = $workers->join('worker_categories','workers.id','worker_categories.worker_id')
            ->select('workers.*')
            ->where(function ($query) use ($request){
                $query->where('worker_categories.category_id','=',$request->catOpt)->distinct();                
            });
        };
        
        return view('worker.index',[
            'workers' => $workers->paginate(10),
            'search'=>$search,
        ]);
    }

    public function show($id){
        return view('worker.show',[
            'worker'=>Worker::findOrFail($id),
        ]);
    }

    public function ascSort(Request $request){
        $search = $request->search??'';
        $catOpt = $request->catOpt??'';
       
        $workers = Worker::join('users as u', 'u.id', '=', 'workers.user_id')
            ->select('workers.*')
            ->distinct()
            ->orderBy('u.name', 'asc');
               
            if($request->has('search')){
                $workers = $workers->join('users','workers.user_id','users.id')
                ->join('worker_categories','workers.id','worker_categories.worker_id')
                ->select('workers.*')
                ->where(function ($query) use ($request){
                    $query->where('users.name','LIKE','%'.$request->search.'%')
                    ->orwhere('workers.description','LIKE','%'.$request->search.'%')->distinct();
                });
            }

        elseif($request->has('catOpt')){
            $workers = $workers->join('worker_categories','workers.id','worker_categories.worker_id')
            ->select('workers.*')
            ->where(function ($query) use ($request){
                $query->where('worker_categories.category_id','=',$request->catOpt)->distinct();                
            });
        };

        return view('worker.index',[
            'workers' => $workers->paginate(10),
            'search'=>$search,
            'catOpt'=>$catOpt
        ]);
    }

    public function descSort(Request $request){
        $search = $request->search??'';
        $catOpt = $request->catOpt??'';


        $workers = Worker::join('users as u', 'u.id', '=', 'workers.user_id')
            ->select('workers.*')
            ->distinct()
            ->orderBy('u.name', 'desc');
        
        if($request->has('search')){
            $workers = $workers->join('users','workers.user_id','users.id')
            ->join('worker_categories','workers.id','worker_categories.worker_id')
            ->select('workers.*')
            ->where(function ($query) use ($request){
                $query->where('users.name','LIKE','%'.$request->search.'%')
                ->orwhere('workers.description','LIKE','%'.$request->search.'%');
            })->distinct();
        }

        elseif($request->has('catOpt')){
            $workers = $workers->join('worker_categories','workers.id','worker_categories.worker_id')
            ->select('workers.*')
            ->where(function ($query) use ($request){
                $query->where('worker_categories.category_id','=',$request->catOpt)->distinct();                
            });
        };

        return view('worker.index',[
            'workers' => $workers->paginate(10),
            'search'=>$search,
            'catOpt'=>$catOpt
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(){
        return view('worker/workerRegister');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        return dd($request);
    }


    /**
     *
     * TESTES
     *
     */

    public function teste_rate($value){
        $av = Avaliation::Create(1,1,1,$value,'');
        return json_encode(
            [
                'Novo valor'=>Worker::find(1)->rate,
                'Total de avaliações'=>Worker::find(1)->avaliations()->count(),
                'Avaliações'=>Worker::find(1)->avaliations,
            ]
        );
    }
}
