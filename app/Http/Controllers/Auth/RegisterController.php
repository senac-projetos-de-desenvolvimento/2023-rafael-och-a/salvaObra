<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Address;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::AFTER_REG_USER;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['string', 'max:20', 'unique:users'],
            'admin_level'=> ['integer', 'in:0,1,2']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'] ? $data['phone'] : null,
            'admin_level' => 0
        ]);

        $user->addresses()->create([
            'street' => $data['street'],
            'neighborhood' => $data['neighborhood'],            
            'city' => $data['city'],
            'uf' => $data['uf'],
            'number' => $data['number'],
            'complement' => $data['complement'],
            'CEP' => $data['CEP'],
        ]);

        //Atualizar o cadastro para obrigar a receber a imagem
        $user->image()->create([
            'src' => '-',
            'alt' => '-',
            'user_id' => $user->id
        ]);

        //Lógica para casdatro de prestador, se houver 
        //um campo 'description', que é required, sabemos
        //que estamos no form de Worker

        if(isset($data['description'])){
            $user->worker()->create([
                'description' => $data['description'],
                'work_phone' => $data['work_phone'],
                'document' => $data['document'],
                'likes' => 0,
                'rate' => 0
            ]);

            //Verifica se alguma categoria foi selecionada
            //gera N queries baseados no work_cat
            if(isset($data['work_cat'])){
                $user->worker->categories()->sync($data['work_cat']);
            }
        }
        return $user;
    }
}
