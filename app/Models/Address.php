<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Address extends Model
{
    use HasFactory;

    protected $fillable=[
        'street','neighborhood',
        'city','uf','number',
        'complement','CEP',
        'user_id'
    ];

    public function rules()
    {
        return [
            'street' => 'required|string|max:255',
            'neighborhood' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'uf' => 'required|string|size:2', // Assuming a 2-character state abbreviation
            'number' => 'required|integer',
            'complement' => 'nullable|string|max:255', // Optional
            'CEP' => 'required|string|max:12',
            //'user_id' => 'required|integer|exists:users,id' // Assuming a foreign key to users table
        ];
    }

    /**
     * Get the user that owns the Address
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    static function Create($request, $user_id = null){
        $request->validate($request);
        $address = new Address();
        $address->street = $request->street;
        $address->neighborhood = $request->neighborhood;
        $address->city = $request->city;
        $address->uf = $request->uf;
        $address->number = $request->number;
        $address->complement = $request->complement;
        $address->CEP = $request->CEP;

        if($user_id===null)
            $address->user_id = $user_id;
        else
            $address->user_id = Auth::check()?Auth::id():null;

        $address->save();
        return $address;
    }
}
