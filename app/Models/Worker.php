<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Worker extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'work_phone',
        'document',
        'description',
        'user_id',
        'rate',
        'likes',
    ];

    /**
     * Get the user that owns the Worker
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all of the categories for the Worker
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, WorkerCategory::class);
    }
/**
     * Get all of the avaliations for the Worker
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function avaliations(): HasMany
    {
        return $this->hasMany(Avaliation::class);
    }


    /**
     *
     * MÉTODOS
     *
     */

    public function likeUnlike($user){
        if(Like::where('user_id',$user->id)->where('worker_id',$this->id)->exists()){
            $this->decrement('likes');
            $like = Like::where('user_id',$user->id)->where('worker_id',$this->id)->first();
            $like = $like->delete();
        }else{
            $this->increment('likes');
            $like = Like::create([
                'user_id' => $user->id,
                'worker_id' => $this->id,
            ]);
        }
        return $like;
    }

    public function RecalculeRate(){
        $rate = 0;
        foreach ($this->avaliations as $avaliation) {
            $rate+=$avaliation->rate;
        }
        $rate = $rate/$this->avaliations()->count();
        $this->rate = $rate;

        $this->save();
        return $rate;
    }
}
