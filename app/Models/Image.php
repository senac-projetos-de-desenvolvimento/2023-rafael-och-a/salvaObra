<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Intervention\Image\Laravel\Facades\Image as ImageManager;


class Image extends Model
{
    use HasFactory;
    protected $fillable =[
        'src',
        'alt',
        'user_id'
    ];

    static public $rules=[
        'src'=>'required|min:5',
    ];

    /**
     * Get the user that owns the Image
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    static function Create($input_image){
        $image = new Image();

        // Crie uma instância do Image
        $img = ImageManager::make($input_image);

        // Obtenha o formato original da imagem
        $format = $input_image->getClientOriginalExtension();

        // Crie um slug único para a imagem
        $slug = Str::slug($input_image->getFilename());

        // Verifique se o slug já existe
        $exists = Image::where('src', $slug)->exists();

        // Se o slug já existir, crie um novo
        while ($exists) {
            $slug .= '-' . rand(0, 9);
            $exists = Image::where('src', $slug)->exists();
        }

        // Salve a versão de 800x600
        $img->resize(800, 600, function ($constraint) {
            $constraint->aspectRatio();
        })->save('public/images/small/' . $slug . '.' . $format,90);

        // Salve a versão full HD
        $img->resize(1920, 1080, function ($constraint) {
            $constraint->aspectRatio();
        })->save('public/images/full/' . $slug . '.' . $format, 90);

        // Atualize a tabela Image com o novo slug
        $image->user_id = Auth::id();
        $image->save(['src' => $slug . '.' . $format]);
        // Retorne a imagem original
        return $image;
    }

}
