<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

class Avaliation extends Model
{
    use HasFactory;

    protected $fillable=[
        'user_id','service_id','image_id','worker_id',
        'rate',
        'description'
    ];

    /**
     * Get the user that owns the Avaliation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the service that owns the Avaliation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service(): BelongsTo
    {
        return $this->belongsTo(Service::class);
    }

    /**
     * Get the worker that owns the Avaliation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function worker(): BelongsTo
    {
        return $this->belongsTo(Worker::class);
    }

    /**
     * Get all of the image for the Avaliation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function image(): BelongsTo
    {
        return $this->belongsTo(Image::class);
    }

    public static function Create($user_id, $service_id, $worker_id, $rate, $description, $image_id=null){
        //Verificações
        if(Avaliation::where('service_id',$service_id)->where('user_id',$user_id)->exists()){
            //Já existe uma avaliação deste usuário para este serviço
            return -1;
        }

        //Fim das verificações

        $avaliation = new Avaliation([
            'user_id'=>$user_id,
            'service_id'=>$service_id,
            'worker_id'=>$worker_id,
            'rate'=>$rate,
            'description'=>$description,
            'image_id'=>$image_id,
        ]);
        $avaliation->save();

        $avaliation->worker->RecalculeRate();
    }
}
