<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'userDescription',
        'workerDescription',
        'accepted',
        'daysToComplete',
        'start_date',
        'user_id',
        'worker_id',
        'category_id',
        'address_id',
        'budget_id',
    ];

    protected $casts = [
        'accepted' => 'boolean',
        'start_date' => 'date',
    ];

    // Relationships
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function worker()
    {
        return $this->belongsTo(Worker::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function parentBudget()
    {
        return $this->belongsTo(Budget::class, 'budget_id');
    }

    public function childBudgets()
    {
        return $this->hasMany(Budget::class, 'budget_id');
    }
}
