<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

//<WorkerCard> --------------------------------------
//
//  - Renderiza um card com os dados de um terminado
//  prestador.
//
//  - Utiliza o componente 'categoryBadge', há um limite (handleBadgeLimit)
//  de até 3 badges diferentes a serem dispostas, caso 
//  um prestador tenha mais que 3 categorias, a última
//  é apresentada como uma badge especial.
//  
//  - Idealmente espera um array de strings contendo
//  os nomes das categorias. Caso receba um array
//  de jsons (flag 'fromJsonArray'), as trata e gera 
//  um novo array de string contendo apenas os nomes
//  das categorias (handleData)
//
//  - Caso o array de categorias esteja vazio, ou seja
//  o prestador não tem nenhuma categoria de serviço 
//  registrada, o mesmo não será renderizado. Este 
//  controle está na view do componente.
//
// >> Parâmetros ::
// 
//      - image: Path da imagem;
//
//      - neighbor: Flag que marca se o prestador
//          é do mesmo bairro do usuário;
//
//      - title/adress/phone/score: Auto-explicativo;
//
//      - categories: Espera um array de nomes de categorias,
//          é utilizado no componente 'categoryBadge'. Caso
//          a flag 'fromJsonArray' seja true, ele utiliza o 
//          método 'handleData' para tratar os dados;
//
//      - badgeLimit: Flag de quantidade de categorias a
//          serem mostradas, seu valor é definido pelo 
//          método 'handleBadgeLimit' 
//
//      - fromJsonArray: Explicado em 'categories'
//
//      - randomImgSeed: Gera um int aleatório, é 
//      utilizado para puxar uma imagem qualquer
//      do picssum, um repositório de imagens.
//      servem como um placeholder para os cards
//      gerador pelo seeder do DB
//
// -----------------------------------------------------------

class WorkerCard extends Component
{
    public $image;
    public $neighbor;
    public $title;
    public $adress;
    public $phone;
    public $categories;
    public $score;
    public $badgeLimit;
    public $fromJsonArray;
    public $randomImgSeed;

    public function __construct($image, $neighbor, $title, $adress, $phone, $categories, $score, $fromJsonArray)
    {
        $this->image = $image;
        $this->neighbor = $neighbor;
        $this->title = $this->handleStringSize($title, 20);
        $this->adress = $this->handleStringSize($adress, 34);
        $this->phone = $phone;
        $this->categories = $this->handleDuplicate($fromJsonArray ? $this->handleData($categories) : $categories);
        $this->score = $score;
        $this->badgeLimit = $this->handleBadgeLimit();
        $this->randomImgSeed = rand();
    }

    public function render(): View|Closure|string
    {
        return view('components.worker-card');
    }

    private function handleStringSize($str, $maxLength){
        if (strlen($str) > (int)$maxLength)
            $str = substr($str, 0, (int)$maxLength-3) . '...';
        elseif(strlen($str) <= 0)
            $str = '-';
        return $str;    
    }

    private function handleBadgeLimit(){
        if(count($this->categories) > 3)
            return true;
        return false;
    }

    private function handleDuplicate($originalArray){
        return array_values(array_unique($originalArray));
    }

    private function handleData($jsonData){
        $catData = [];
        if(count($jsonData) > 0 ){
            foreach($jsonData as $json){
                $catData[] = $json->name;
            }
        }
        return $catData;
    }
}


