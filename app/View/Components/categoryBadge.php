<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

//<Categorybadge> --------------------------------------
//
//  Componente responsável pela renderização de
//  badges de categorias válidas.
//
//  Cada icone tem um tamanho específico, 
//  controlado pelo método 'inconSize()'
//
//  Verifica se o $category recebido é valido, 
//  comparando com as opções em $catList, caso 
//  não seja, (!) não renderiza nada (!).
//
//
// >> Exemplo de utilização ::
//       <x-category-badge :category="$data = 'servente'" :hasName=true :profileText=true />
//
//
// >> Parâmetros :: 
//      - category : nome da categoria, busca
//          ícone com a mesma assinatura.
//
//      - hasName : Boolean que define se o
//          nome da assinatura estará visível
//
//      - profileText : Define a cor da fonte do 
//          display de nome da categoria, assim como
//          seu tamanho.
//
// -----------------------------------------------------------


class categoryBadge extends Component
{
    private $catList = [
        'arquitetura',
        'demolicao',
        'eletricista',
        'empreiteira',
        'encanamento',
        'engenharia',
        'favorito',
        'frete',
        'marcenaria',
        'pedreiro',
        'pintura',
        'servente', 
        'outros'
    ];
    
    public $category;
    public $hasName;
    public $profileText;
    public $iconW = "10px";
    public $iconH = "10px";

    /**
     * Create a new component instance.
     */
    public function __construct($category, $hasName, $profileText)
    {
        $this->hasName = $hasName;
        $this->profileText = $profileText;
        
        $category = strtolower($category);

        //Verifica se o param $category existe em $catList
        $this->category = in_array($category, $this->catList) ? $category : 'invalid';

        $this->iconSize($this->category);
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.category-badge');
    }

    private function iconSize($category){
        switch($category){
            case 'arquitetura':
                $this->iconW = '20px';
                $this->iconH = '19px';
                break;
            case 'demolicao':
                $this->iconW = '19px';
                $this->iconH = '21px';
                break;
            case 'eletricista':
                $this->iconW = '17px';
                $this->iconH = '21px';
                break;    
            case 'empreiteira':
                $this->iconW = '12px';
                $this->iconH = '22px';
                break;
            case 'encanamento':
                $this->iconW = '16px';
                $this->iconH = '16px';
                break;
            case 'engenharia':
                $this->iconW = '19px';
                $this->iconH = '19px';
                break;    
            case 'favorito':
                $this->iconW = '22px';
                $this->iconH = '16px';
                break;
            case 'frete':
                $this->iconW = '21px';
                $this->iconH = '21px';
                break;
            case 'marcenaria':
                $this->iconW = '16px';
                $this->iconH = '19px';
                break;    
            case 'pedreiro':
                $this->iconW = '19px';
                $this->iconH = '16px';
                break;
            case 'pintura':
                $this->iconW = '13px';
                $this->iconH = '15px';
                break;
            case 'servente':
                $this->iconW = '17px';
                $this->iconH = '24px';
                break;    
            case 'outros':
                $this->iconW = '17px';
                $this->iconH = '24px';
                break;
        }
    }
}
