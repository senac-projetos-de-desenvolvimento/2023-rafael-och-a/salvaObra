<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\AlbumController;
use App\Http\Controllers\AvaliationController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DefaultController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WorkerController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[App\Http\Controllers\HomeController::class,'home'])->name('home');

Auth::routes();

Route::prefix('/afterReg')->group(function(){
    Route::get('/client', function(){
        return view('templates/registerCongratulations');
    })->name('afterReg.client');
});

Route::prefix('worker')->group(function () {
    Route::get('/', [WorkerController::class, 'index'])->name('worker.index');
    Route::get('/az', [WorkerController::class, 'ascSort'])->name('worker.az');
    Route::get('/za', [WorkerController::class, 'descSort'])->name('worker.za');
    Route::get('/create', [WorkerController::class, 'create'])->name('worker.create');
    Route::post('/store', [WorkerController::class, 'store'])->name('worker.store');
    Route::get('/{id}', [WorkerController::class, 'show'])->name('worker.show')->middleware('auth');
});

Route::prefix('admin')->group(function () {
    Route::prefix('album')->group(function () {
        Route::get('/', [AlbumController::class, 'adminIndex'])->name('admin.album.index');
        Route::post('/store', [AlbumController::class, 'adminStore'])->name('admin.album.store');
        Route::post('/update/{id}', [AlbumController::class, 'adminUpdate'])->name('admin.album.update');
        Route::post('/delete/{id}', [AlbumController::class, 'adminDelete'])->name('admin.album.delete');
    });
    Route::prefix('address')->group(function () {
        Route::get('/', [AddressController::class, 'adminIndex'])->name('admin.address.index');
        Route::post('/store', [AddressController::class, 'adminStore'])->name('admin.address.store');
        Route::post('/update/{id}', [AddressController::class, 'adminUpdate'])->name('admin.address.update');
        Route::post('/delete/{id}', [AddressController::class, 'adminDelete'])->name('admin.address.delete');
    });
    Route::prefix('avaliation')->group(function () {
        Route::get('/', [AvaliationController::class, 'adminIndex'])->name('admin.avaliation.index');
        Route::post('/store', [AvaliationController::class, 'adminStore'])->name('admin.avaliation.store');
        Route::post('/update/{id}', [AvaliationController::class, 'adminUpdate'])->name('admin.avaliation.update');
        Route::post('/delete/{id}', [AvaliationController::class, 'adminDelete'])->name('admin.avaliation.delete');
    });
    Route::prefix('category')->group(function () {
        Route::get('/', [CategoryController::class, 'adminIndex'])->name('admin.category.index');
        Route::post('/store', [CategoryController::class, 'adminStore'])->name('admin.category.store');
        Route::post('/update/{id}', [CategoryController::class, 'adminUpdate'])->name('admin.category.update');
        Route::post('/delete/{id}', [CategoryController::class, 'adminDelete'])->name('admin.category.delete');
    });
    Route::prefix('user')->group(function () {
        Route::get('/', [UserController::class, 'adminIndex'])->name('admin.user.index');
        Route::post('/store', [UserController::class, 'adminStore'])->name('admin.user.store');
        Route::post('/update/{id}', [UserController::class, 'adminUpdate'])->name('admin.user.update');
        Route::post('/delete/{id}', [UserController::class, 'adminDelete'])->name('admin.user.delete');
    });
    Route::prefix('worker')->group(function () {
        Route::get('/', [WorkerController::class, 'adminIndex'])->name('admin.worker.index');
        Route::post('/store', [WorkerController::class, 'adminStore'])->name('admin.worker.store');
        Route::post('/update/{id}', [WorkerController::class, 'adminUpdate'])->name('admin.worker.update');
        Route::post('/delete/{id}', [WorkerController::class, 'adminDelete'])->name('admin.worker.delete');
    });

    Route::get('/', [UserController::class, 'adminDashboard'])->name('admin.dashboard');
    Route::post('/update', [UserController::class, 'adminUpdate'])->name('admin.update');
});

Route::prefix('user')->group(function () {
    Route::prefix('album')->group(function () {
        Route::get('/', [AlbumController::class, 'index'])->name('user.album.index');
        Route::post('/store', [AlbumController::class, 'store'])->name('user.album.store');
        Route::post('/update/{id}', [AlbumController::class, 'update'])->name('user.album.update');
        Route::post('/delete/{id}', [AlbumController::class, 'delete'])->name('user.album.delete');
    });
    Route::prefix('avaliation')->group(function () {
        Route::get('/', [AvaliationController::class, 'index'])->name('user.avaliation.index');
        Route::post('/store', [AvaliationController::class, 'store'])->name('user.avaliation.store');
        Route::post('/update/{id}', [AvaliationController::class, 'update'])->name('user.avaliation.update');
        Route::post('/delete/{id}', [AvaliationController::class, 'delete'])->name('user.avaliation.delete');
    });
    Route::prefix('worker')->group(function () {
        Route::get('/', [WorkerController::class, 'index'])->name('user.worker.index');
        Route::post('/store', [WorkerController::class, 'store'])->name('user.worker.store');
        Route::post('/update/{id}', [WorkerController::class, 'update'])->name('user.worker.update');
        Route::post('/delete/{id}', [WorkerController::class, 'delete'])->name('user.worker.delete');
    });

    //Teste de profile próprio do usuário
    Route::get('/profile', [UserController::class, 'profile'])->name('user.profile')->middleware('auth');
    Route::get('/', [UserController::class, 'dashboard'])->name('user.dashboard')->middleware('auth');
    Route::get('/edit/{id}', [UserController::class, 'edit'])->name('user.edit')->middleware('auth');
    Route::post('/update/{id}', [UserController::class, 'update'])->name('user.update')->middleware('auth');
});

//Seleção de tipo de usuário antes de cadastro
// Cliente | Prestador
Route::get('/accTypeSel', function(){
    return view('accTypeSel');
})->name('accTypeSel');

Route::get('/paginaTeste', function(){
    return view('/templates/generalPagesTemplate');
});

/**
 *
 * TESTES
 * (Roda apenas em localhost)
 *
 */

 if (!app()->isProduction()) {
    Route::get('/teste/rate/{value}', [WorkerController::class, 'teste_rate'])->name('teste.worker.rate');
 }
